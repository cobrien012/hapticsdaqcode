# -*- coding: utf-8 -*-
"""
Created on Thu May 15 12:08:41 2014

@author: conor
"""
import pylab as pl
import serial

def toInt(x):
	return ((ord(x[0])<<8) + ord(x[1]))*2

ser = serial.Serial('/dev/tty.usbmodem22491', 6900, timeout=0.1)
ser.flush()
x = []
y = []
z = []

n = 2000*30
pl.ion()
pl.axis([0,500,0,4096])

xline, = pl.plot([2048] * 500)
yline, = pl.plot([2048] * 500)
zline, = pl.plot([2048] * 500)

pl.draw
for i in range(1, n):
	dat = ser.read(7)
	
	if dat == []:
		print 'nothing recieved'
		break
	
	j = 0
	x.append(toInt(dat[j:(j+2)]))
	#x.append(ord(dat[j+1]))	
	j = 2
	y.append(toInt(dat[j:(j+2)]))
	#y.append(ord(dat[j+1]))	
	j = 4
	z.append(toInt(dat[j:(j+2)]))
	#z.append(ord(dat[j+1]))	

	if i % 100 == 0:

		if i >= 500:
			xline.set_ydata(x[(len(x) - 500):len(x)])
			yline.set_ydata(y[(len(y) - 500):len(y)])
			zline.set_ydata(z[(len(z) - 500):len(z)])

		pl.draw()





# pl.plot(x, 'r')
# pl.plot(y, 'g')
# pl.plot(z, 'b')
# pl.axis([0, n, 0, 4096])
# pl.show()
ser.close()