#include <IntervalTimer.h>
#include <SPI.h>

#define RESET 			0x10
#define SETUP_ACC 		0x64
#define CONV_ACC 		0xC0
#define CS_ACC 			9
#define SETUP_FT		0x6B
#define CONV_FT 		0xD8
#define CS_FT			10
#define SAMPLE_RATE 	500 // Delay in us between samples

IntervalTimer irq;
char data[30];
byte j = 0;

void readADC_ACC(void)
{
	// Right now only reads from acc ADC, will add F/T ADC this weekend
	Serial.flush();

	// Read 9 bytes from ADC, conv. bit has already been sen
	for (int i = 0; i < 9; i++)
	{	
		digitalWrite(CS_ACC, LOW);
		data[i] = SPI.transfer(0x00);
		digitalWrite(CS_ACC, HIGH);
		delayMicroseconds(2);
	}

	digitalWrite(CS_ACC, HIGH);

	// Last byte of package is increments, check for lost packets on master side
	data[i+1] = j++;

	// Write data theough USB
	Serial.write(data, (i+1));

	// Send conv start byte for next IRQ
	digitalWrite(CS_ACC, LOW);
	SPI.transfer(CONV_ACC);
	digitalWrite(CS_ACC, HIGH);

}

void pulseCS(char pin)
{
	// Pulses the CS line in between SPI bytes
	digitalWrite(pin, HIGH);
	delayMicroseconds(2);
	digitalWrite(pin, LOW);
}

void setup(void)
{

	// Declare chip select pins, set to idle high
	pinMode(CS_ACC, OUTPUT);
	pinMode(CS_FT, OUTPUT);
	digitalWrite(CS_FT, HIGH);
	digitalWrite(CS_ACC, HIGH);

	// Start SPI, 8Mhz speed, Defaults to mode 0
	SPI.begin();
	SPI.setClockDivider(SPI_CLOCK_DIV2);

	// Setup for Accelerometer ADC, resets, writes setup byte, then starts first conversion
	digitalWrite(CS_ACC, LOW);
	SPI.transfer(RESET);
	pulseCS(CS_ACC);
	SPI.transfer(SETUP_ACC);
	pulseCS(CS_ACC);
	SPI.transfer(CONV_ACC);
	digitalWrite(CS_ACC, HIGH);

	// Short delay to let everything settle
	delay(100);

	// Start USB
	Serial.begin(9600);

	//Start timer interrupt
	irq.begin(readADC_ACC, SAMPLE_RATE);

}

void loop(void)
{

}